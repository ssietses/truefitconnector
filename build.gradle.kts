plugins {
	id("java")
	id("org.sonarqube").version("2.6.2")
	id("org.springframework.boot").version("2.1.1.RELEASE")
	id("io.spring.dependency-management").version("1.0.6.RELEASE")
}

val springCloudVersion = "Greenwich.BUILD-SNAPSHOT"

group = "com.bestseller.services"
version = "0.0.1-SNAPSHOT"

java {
	sourceCompatibility = JavaVersion.VERSION_11
}

repositories {
	mavenCentral()
	maven("https://repo.spring.io/snapshot")
	maven("https://repo.spring.io/milestone")
}

dependencies {
	compile("io.micrometer:micrometer-registry-prometheus")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.cloud:spring-cloud-stream")
	testCompile("org.springframework.boot:spring-boot-starter-test")
	testCompile("io.projectreactor:reactor-test")
	testCompile("org.springframework.cloud:spring-cloud-stream-test-support")
}

dependencyManagement {
	imports {
		mavenBom("org.springframework.cloud:spring-cloud-dependencies:$springCloudVersion")
	}
}